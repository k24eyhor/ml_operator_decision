'''
Source of the code: https://github.com/Romaric-Moyeuvre/NewOperatorDecision/tree/main
Author: Romaric moyeuvre, in the framework of the Project ODM 
Additional information about the project can be found in additional ressource
'''

import os
import numpy as np
import netCDF4
import pandas as pd
from tqdm import tqdm
import math
import argparse


def parse_arguments():
    parser = argparse.ArgumentParser(description="Process dataset and save statistics.")
    parser.add_argument(
        "--dataset_path",
        type=str,
        default="Learning_Dataset_2_v1/v1",
        help="Path to the dataset",
    )
    parser.add_argument(
        "--save_path",
        type=str,
        default="dataset_pandas/dataset_v2_v1.pkl",
        help="Path to save the dataset",
    )
    return parser.parse_args()


def get_stats(matrix):
    """Get the statistics of the dataset for each profile."""
    num_measurements = len(matrix[0])
    stats = {
        "abs_S_Smin": 0,
        "rel_S_Smin_semi_width": 0,
        "rel_S_Smin_full_width": 0,
        "abs_S_Smax": 0,
        "rel_S_Smax_semi_width": 0,
        "rel_S_Smax_full_width": 0,
        "count_anomalies_S": 0,
        "ratio_anomalies_S": 0,
        "max_variation_S": 0,
        "abs_T_Tmin": 0,
        "rel_T_Tmin_semi_width": 0,
        "rel_T_Tmin_full_width": 0,
        "abs_T_Tmax": 0,
        "rel_T_Tmax_semi_width": 0,
        "rel_T_Tmax_full_width": 0,
        "count_anomalies_T": 0,
        "ratio_anomalies_T": 0,
        "max_variation_T": 0,
        "mean_correlation": 0,
        "nb_measurements": num_measurements,
    }

    if num_measurements == 1:
        compute_single_measurement_stats(matrix, stats)
    else:
        compute_multiple_measurements_stats(matrix, stats, num_measurements)

    return list(stats.values())


def compute_single_measurement_stats(matrix, stats):
    P, S, Smin, Smed, Smax, T, Tmin, Tmed, Tmax = (matrix[i][0] for i in range(9))

    stats["abs_S_Smin"] = abs(S - Smin)
    stats["rel_S_Smin_semi_width"] = (
        abs(S - Smin) / abs(Smed - Smin) if Smed != Smin else abs(S - Smin)
    )
    stats["rel_S_Smin_full_width"] = (
        abs(S - Smin) / abs(Smax - Smin) if Smax != Smin else abs(S - Smin)
    )
    stats["abs_S_Smax"] = abs(S - Smax)
    stats["rel_S_Smax_semi_width"] = (
        abs(S - Smax) / abs(Smed - Smax) if Smed != Smax else abs(S - Smax)
    )
    stats["rel_S_Smax_full_width"] = (
        abs(S - Smax) / abs(Smin - Smax) if Smin != Smax else abs(S - Smax)
    )
    stats["count_anomalies_S"] = int(S < Smin or S > Smax)
    stats["ratio_anomalies_S"] = int(S < Smin or S > Smax)

    stats["abs_T_Tmin"] = abs(T - Tmin)
    stats["rel_T_Tmin_semi_width"] = (
        abs(T - Tmin) / abs(Tmed - Tmin) if Tmed != Tmin else abs(T - Tmin)
    )
    stats["rel_T_Tmin_full_width"] = (
        abs(T - Tmin) / abs(Tmax - Tmin) if Tmax != Tmin else abs(T - Tmin)
    )
    stats["abs_T_Tmax"] = abs(T - Tmin)
    stats["rel_T_Tmax_semi_width"] = (
        abs(T - Tmax) / abs(Tmed - Tmax) if Tmed != Tmax else abs(T - Tmax)
    )
    stats["rel_T_Tmax_full_width"] = (
        abs(T - Tmax) / abs(Tmin - Tmax) if Tmin != Tmax else abs(T - Tmax)
    )
    stats["count_anomalies_T"] = int(T < Tmin or T > Tmax)
    stats["ratio_anomalies_T"] = int(T < Tmin or T > Tmax)


def compute_multiple_measurements_stats(matrix, stats, num_measurements):
    for k in range(num_measurements):
        P, S, Smin, Smed, Smax, T, Tmin, Tmed, Tmax = (matrix[i][k] for i in range(9))

        stats["abs_S_Smin"] = max(abs(S - Smin), stats["abs_S_Smin"])
        stats["rel_S_Smin_semi_width"] = (
            max(abs(S - Smin) / abs(Smed - Smin), stats["rel_S_Smin_semi_width"])
            if Smed != Smin
            else stats["rel_S_Smin_semi_width"]
        )
        stats["rel_S_Smin_full_width"] = (
            max(abs(S - Smin) / abs(Smax - Smin), stats["rel_S_Smin_full_width"])
            if Smax != Smin
            else stats["rel_S_Smin_full_width"]
        )
        stats["abs_S_Smax"] = max(abs(S - Smax), stats["abs_S_Smax"])
        stats["rel_S_Smax_semi_width"] = (
            max(abs(S - Smax) / abs(Smed - Smax), stats["rel_S_Smax_semi_width"])
            if Smed != Smax
            else stats["rel_S_Smax_semi_width"]
        )
        stats["rel_S_Smax_full_width"] = (
            max(abs(S - Smax) / abs(Smin - Smax), stats["rel_S_Smax_full_width"])
            if Smin != Smax
            else stats["rel_S_Smax_full_width"]
        )
        stats["count_anomalies_S"] += int(S < Smin or S > Smax)

        stats["abs_T_Tmin"] = max(abs(T - Tmin), stats["abs_T_Tmin"])
        stats["rel_T_Tmin_semi_width"] = (
            max(abs(T - Tmin) / abs(Tmed - Tmin), stats["rel_T_Tmin_semi_width"])
            if Tmed != Tmin
            else stats["rel_T_Tmin_semi_width"]
        )
        stats["rel_T_Tmin_full_width"] = (
            max(abs(T - Tmin) / abs(Tmax - Tmin), stats["rel_T_Tmin_full_width"])
            if Tmax != Tmin
            else stats["rel_T_Tmin_full_width"]
        )
        stats["abs_T_Tmax"] = max(abs(T - Tmax), stats["abs_T_Tmax"])
        stats["rel_T_Tmax_semi_width"] = (
            max(abs(T - Tmax) / abs(Tmed - Tmax), stats["rel_T_Tmax_semi_width"])
            if Tmed != Tmax
            else stats["rel_T_Tmax_semi_width"]
        )
        stats["rel_T_Tmax_full_width"] = (
            max(abs(T - Tmax) / abs(Tmin - Tmax), stats["rel_T_Tmax_full_width"])
            if Tmin != Tmax
            else stats["rel_T_Tmax_full_width"]
        )
        stats["count_anomalies_T"] += int(T < Tmin or T > Tmax)

    for k in range(num_measurements - 1):
        P_k, S_k, T_k, P_k2, S_k2, T_k2 = (matrix[i][k] for i in (0, 1, 5, 0, 1, 5))

        stats["max_variation_S"] = (
            max(
                (abs(S_k - S_k2) * abs(P_k + P_k2))
                / (abs(S_k + S_k2) * abs(P_k - P_k2)),
                stats["max_variation_S"],
            )
            if abs(S_k + S_k2) * abs(P_k - P_k2)
            else stats["max_variation_S"]
        )
        stats["max_variation_T"] = (
            max(
                (abs(T_k - T_k2) * abs(P_k + P_k2))
                / (abs(T_k + T_k2) * abs(P_k - P_k2)),
                stats["max_variation_T"],
            )
            if abs(T_k + T_k2) * abs(P_k - P_k2)
            else stats["max_variation_T"]
        )
        stats["mean_correlation"] += abs(S_k * T_k2 - S_k2 * T_k) / num_measurements

    stats["ratio_anomalies_S"] = stats["count_anomalies_S"] / num_measurements
    stats["ratio_anomalies_T"] = stats["count_anomalies_T"] / num_measurements


def filter_matrix(matrix):
    """Filter the matrix to remove invalid measurements."""
    valid_indices = [
        k
        for k in range(len(matrix[0]))
        if not any(math.isnan(matrix[i][k]) for i in range(len(matrix)))
    ]
    return matrix[:, valid_indices]


def sort_matrix(matrix):
    """Sort the matrix by the first row (pressure)."""
    return matrix[:, np.argsort(matrix[0])]


def pad_matrix(matrix, target_length=3000):
    """Pad the matrix to a target length with zeros."""
    pad_width = target_length - matrix.shape[1]
    if pad_width > 0:
        return np.pad(matrix, ((0, 0), (0, pad_width)), mode="constant")
    return matrix


def create_measures_dataset(dataset_path, save_path):
    """Create the dataset with the measures of the profiles."""
    df = pd.DataFrame(
        columns=[
            "id",
            "abs_S_Smin",
            "rel_S_Smin_semi_width",
            "rel_S_Smin_full_width",
            "abs_S_Smax",
            "rel_S_Smax_semi_width",
            "rel_S_Smax_full_width",
            "count_anomalies_S",
            "ratio_anomalies_S",
            "max_variation_S",
            "abs_T_Tmin",
            "rel_T_Tmin_semi_width",
            "rel_T_Tmin_full_width",
            "abs_T_Tmax",
            "rel_T_Tmax_semi_width",
            "rel_T_Tmax_full_width",
            "count_anomalies_T",
            "ratio_anomalies_T",
            "max_variation_T",
            "mean_correlation",
            "nb_measurements",
            "alarm_temp",
            "alarm_salinity",
            "FalseorTrue_T",
            "FalseorTrue_S",
        ]
    )

    for file in tqdm(os.listdir(dataset_path)):
        if not file.endswith(".nc"):
            continue

        data = netCDF4.Dataset(os.path.join(dataset_path, file), "r", format="NETCDF4")
        num_profiles = data["PARAM"].shape[0]

        for idx in range(num_profiles):
            profile_data = extract_profile_data(data, idx)
            if profile_data:
                profile_id, stats, alarms = profile_data
                profile_df = pd.DataFrame(
                    {**stats, **alarms, "id": profile_id}, index=[0]
                )
                df = pd.concat([df, profile_df], ignore_index=True)

    df.set_index("id", inplace=True)
    df.to_pickle(save_path)


def extract_profile_data(data, idx):
    """Extract profile data and compute statistics."""
    param = data["PARAM"][idx]
    alarm_temp = int(param in (0.0, 2.0))
    alarm_salinity = int(param in (1.0, 2.0))

    profile_data = {
        "FalseorTrue_T": int(data["FALSEorTRUE_T"][idx]),
        "FalseorTrue_S": int(data["FALSEorTRUE_S"][idx]),
        "pressure": np.array(data["PRES"][idx][:]),
        "psal": np.array(data["PSAL"][idx][:]),
        "psal_min": np.array(data["PSAL_MIN"][idx][:]),
        "psal_med": np.array(data["PSAL_MED"][idx][:]),
        "psal_max": np.array(data["PSAL_MAX"][idx][:]),
        "temp": np.array(data["TEMP"][idx][:]),
        "temp_min": np.array(data["TEMP_MIN"][idx][:]),
        "temp_med": np.array(data["TEMP_MED"][idx][:]),
        "temp_max": np.array(data["TEMP_MAX"][idx][:]),
    }

    matrix = np.array(
        [
            profile_data[key]
            for key in [
                "pressure",
                "psal",
                "psal_min",
                "psal_med",
                "psal_max",
                "temp",
                "temp_min",
                "temp_med",
                "temp_max",
            ]
        ]
    )
    matrix = filter_matrix(matrix)
    if matrix.shape[1] == 0:
        return None

    matrix = sort_matrix(matrix)
    matrix = np.vstack([matrix, np.ones((1, matrix.shape[1]))])  # Add information row
    matrix = pad_matrix(matrix)

    stats = dict(
        zip(
            [
                "abs_S_Smin",
                "rel_S_Smin_semi_width",
                "rel_S_Smin_full_width",
                "abs_S_Smax",
                "rel_S_Smax_semi_width",
                "rel_S_Smax_full_width",
                "count_anomalies_S",
                "ratio_anomalies_S",
                "max_variation_S",
                "abs_T_Tmin",
                "rel_T_Tmin_semi_width",
                "rel_T_Tmin_full_width",
                "abs_T_Tmax",
                "rel_T_Tmax_semi_width",
                "rel_T_Tmax_full_width",
                "count_anomalies_T",
                "ratio_anomalies_T",
                "max_variation_T",
                "mean_correlation",
                "nb_measurements",
            ],
            get_stats(matrix),
        )
    )

    profile_id = f"{os.path.splitext(os.path.basename(data.filepath()))[0]}_{idx}"
    alarms = {
        "alarm_temp": alarm_temp,
        "alarm_salinity": alarm_salinity,
        "FalseorTrue_T": profile_data["FalseorTrue_T"],
        "FalseorTrue_S": profile_data["FalseorTrue_S"],
    }

    return profile_id, stats, alarms


if __name__ == "__main__":
    args = parse_arguments()
    create_measures_dataset(args.dataset_path, args.save_path)
