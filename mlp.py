import torch.nn as nn
import pandas as pd
import numpy as np
import torch
from tqdm import trange, tqdm
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import seaborn as sns
import matplotlib.pyplot as plt
import torch.nn.functional as F
from dataloader import DataLoader


RANDOM_SEED = 123122334

##### HYPERPARAMETERS #####
EPOCHS = 200
BATCH_SIZE = 32
LEARNING_RATE = 1e-4
GROWTH_RATE = 16

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class MLP(nn.Module):
    """
    MLP Definiation as taken from the paper of Romaric
    """

    def __init__(self, input_features, growth_rate):
        super(MLP, self).__init__()
        self.block1 = nn.Sequential(
            nn.Linear(input_features, GROWTH_RATE),
            nn.ReLU(),
        )
        self.block2 = nn.Sequential(
            nn.Linear(growth_rate, growth_rate),
            nn.ReLU(),
        )
        self.block3 = nn.Sequential(
            nn.Linear(2 * growth_rate, growth_rate),
            nn.ReLU(),
        )
        self.block4 = nn.Sequential(
            nn.Linear(3 * growth_rate, growth_rate),
            nn.ReLU(),
        )
        self.block5 = nn.Sequential(
            nn.Linear(4 * growth_rate, growth_rate),
            nn.ReLU(),
        )
        self.block6 = nn.Sequential(
            nn.Linear(5 * growth_rate, growth_rate),
            nn.ReLU(),
        )
        self.output = nn.Linear(growth_rate, 1)

    def forward(self, x):
        x1 = self.block1(x)
        x2 = self.block2(x1)
        x3 = self.block3(torch.cat([x1, x2], dim=1))
        x4 = self.block4(torch.cat([x1, x2, x3], dim=1))
        x5 = self.block5(torch.cat([x1, x2, x3, x4], dim=1))
        x6 = self.block6(torch.cat([x1, x2, x3, x4, x5], dim=1))
        y = self.output(x3)
        y = torch.sigmoid(y)
        return y


class MLPWrapper:
    """
    Wrapper class to encapsulate the training of the Model
    """

    def __init__(
        self,
        input_features,
        train_loader,
        test_loader,
        model_seed,
        learning_rate=1e-4,
        device="cpu",
        growth_rate=16,
    ):
        """
        Constructor for the MLPWrapper class

        Parameters:
        -----------
        input_features : int
            The number of input features
        train_loader : torch.utils.data.DataLoader
            The training DataLoader object
        test_loader : torch.utils.data.DataLoader
            The test DataLoader object
        model_seed : int
            The seed to use for reproducibility (Weights initialization)
        learning_rate : float
            The learning rate to use for the optimizer (default=1e-4)
        device : str
            The device to use for training (default='cpu')
        growth_rate : int
            The growth rate to use for the model (default=16)
        """
        self.input_features = input_features
        self.growth_rate = growth_rate
        self.train_loader = train_loader
        self.test_loader = test_loader
        self.learning_rate = learning_rate
        self.model_seed = model_seed
        torch.manual_seed(model_seed)
        self.model = MLP(input_features=input_features, growth_rate=growth_rate).to(
            device
        )
        self.train_losses = []
        self.test_losses = []
        self.train_accuracies = []
        self.test_accuracies = []
        self.criterion = nn.BCELoss()
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=learning_rate)
        self.scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
            self.optimizer,
            mode="min",
            factor=0.1,
            patience=10,
            eps=1e-8,
        )

    def train(self, epochs):
        """
        Train the model for a given number of epochs

        Parameters:
        -----------
        epochs : int
            The number of epochs to train the model
        """
        for epoch in range(epochs):
            epoch_train_loss = 0
            total_train = 0
            correct_train = 0
            with tqdm(self.train_loader, unit="batch") as t:
                self.model.train()
                for data, target in t:
                    t.set_description(f"Epoch {str(epoch).rjust(5)}")
                    data, target = data.to(device), target.to(device)
                    output = self.model(data.float())
                    loss = self.criterion(output, target.float().view(-1, 1))
                    epoch_train_loss += loss.item()
                    self.optimizer.zero_grad()
                    loss.backward()
                    self.optimizer.step()
                    y_train_pred_binary = np.where(output.data > 0.5, 1, 0).reshape(
                        1, len(output)
                    )[0]
                    correct_train += (
                        (target.numpy() == y_train_pred_binary).sum().item()
                    )
                    total_train += len(target)
                    t.set_postfix(train_loss=f"{loss.item():.4f}")
            train_accuracy = 100 * correct_train / total_train
            self.train_accuracies.append(train_accuracy)
            print(f"Train accuracy: {train_accuracy}")
            print(f"Learning rate: {self.optimizer.param_groups[0]['lr']}")
            epoch_train_loss /= len(self.train_loader)
            self.train_losses.append(epoch_train_loss)
            self.scheduler.step(epoch_train_loss)
            self.model.eval()
            with torch.no_grad():
                test_loss = 0
                correct_test = 0
                total_test = 0
                for data, target in self.test_loader:
                    data, target = data.to(device), target.to(device)
                    output = self.model(data.float())
                    loss = self.criterion(output, target.float().view(-1, 1))
                    test_loss += loss.item()
                    y_test_pred_binary = np.where(output.data > 0.5, 1, 0).reshape(
                        1, len(output)
                    )[0]
                    correct_test += (target.numpy() == y_test_pred_binary).sum().item()
                    total_test += len(target)
                test_loss /= len(self.test_loader)
                test_accuracy = 100 * correct_test / total_test
                self.test_accuracies.append(test_accuracy)
                self.test_losses.append(test_loss)
                print(f"Test accuracy: {test_accuracy}")
                print(f"Test loss: {test_loss:.4f}")

    def load_model_from_checkpoint(self, path="checkpoints/mlp.pth"):
        """
        Load the model from a checkpoint

        Parameters:
        -----------
        path : str
            The path to the checkpoint file
        """
        print(f"Loading model from checkpoint: mlp.pth")
        self.model.load_state_dict(torch.load(path))

    def save_checkpoint(self, path="checkpoints/mlp.pth"):
        """
        Save the model to a checkpoint

        Parameters:
        -----------
        path : str
            The path to save the checkpoint file
        """
        torch.save(
            self.model.state_dict(),
            path,
        )


########## POST TRAINING & EVALUATION HELPERS############
def get_evaluation(model, X_test) -> tuple[np.array, np.array]:
    """
    Function to predict on the test set and return the predictions in raw and binary form

    Parameters:
    -----------
    model : torch.nn.Module
        The trained model
    X_test : np.array
        The test features

    Returns:
    --------

    y_test_pred : np.array
        The raw predictions
    y_test_pred_binary : np.array
        The binary predictions
    """
    model.eval()
    with torch.no_grad():
        y_test_pred = (
            model(torch.tensor(X_test).float().to(device)).cpu().detach().numpy()
        )
    y_test_pred_binary = np.where(y_test_pred > 0.5, 1, 0)

    return y_test_pred, y_test_pred_binary


def get_metrics(cm) -> tuple[float, float, float, float, float]:
    """
    Function to calculate the accuracy, precision, recall, F1 and F2 scores from a confusion matrix

    Parameters:
    -----------
    cm : np.array
        The confusion matrix
    """

    # calculate accuracy
    accuracy = np.sum(np.diag(cm)) / np.sum(cm)
    print(f"Accuracy: {accuracy}")

    # calculate recall
    recall = cm[1, 1] / (cm[1, 0] + cm[1, 1])
    print(f"Recall: {recall}")

    # calculate precision
    precision = cm[1, 1] / (cm[0, 1] + cm[1, 1])
    print(f"Precision: {precision}")

    # calculate F1 score
    f1 = 2 * (precision * recall) / (precision + recall)
    print(f"F1 score: {f1}")

    # calculate F2 score
    f2 = 5 * (precision * recall) / (4 * precision + recall)
    print(f"F2 score: {f2}")

    return accuracy, precision, recall, f1, f2


def sort_testdata_into_cm(test_df, y_test_pred, y_test_pred_binary):
    """
    Function to sort the test data into the confusion matrix categories

    Parameters:
    -----------
    test_df : pd.DataFrame
        The test DataFrame
    y_test_pred : np.array
        The raw predictions
    y_test_pred_binary : np.array
        The binary predictions

    Returns:
    --------
    results : pd.DataFrame
        The DataFrame with the predictions sorted into the confusion matrix categories
    """

    # Function to determine TP, TN, FP, FN
    def determine_result(row):
        if row["FalseorTrue"] == 1 and row["predicted"] == 1:
            return "TP"
        elif row["FalseorTrue"] == 0 and row["predicted"] == 0:
            return "TN"
        elif row["FalseorTrue"] == 0 and row["predicted"] == 1:
            return "FP"
        elif row["FalseorTrue"] == 1 and row["predicted"] == 0:
            return "FN"

    # Sort the predictions into CM categories
    y_pred_series = pd.Series(y_test_pred.reshape(1, -1)[0])
    y_pred_binary_series = pd.Series(y_test_pred_binary.reshape(1, -1)[0])
    results = test_df.copy()
    y_pred_series.index = results.index
    y_pred_binary_series.index = results.index
    results["predicted"] = y_pred_binary_series.astype(bool)
    results["predicted_raw"] = y_pred_series

    # Create new column based on conditions for the confusion matrix
    results["CM"] = results.apply(determine_result, axis=1)

    return results


if __name__ == "__main__":

    # Create a DataLoader object with the desired parameters
    dl = DataLoader(
        variant="v1",
        features="both",
        batch_size=BATCH_SIZE,
        seed=RANDOM_SEED,
        rebalance_train=True,
        rebalance_test=False,
    )

    # Create an MLPWrapper object with the desired parameters
    mlpw = MLPWrapper(
        input_features=dl.num_features,
        growth_rate=GROWTH_RATE,
        train_loader=dl.train_loader,
        test_loader=dl.test_loader,
        learning_rate=LEARNING_RATE,
        model_seed=RANDOM_SEED,
        device=device,
    )

    # Train the model
    mlpw.train(epochs=EPOCHS)

    # Get the predictions on the test set
    y_test_pred, y_test_pred_binary = get_evaluation(mlpw.model, dl.X_test)

    # Get and Plot the confusion matrix
    cm = confusion_matrix(y_test_pred_binary, dl.y_test)
    sns.heatmap(cm, annot=True, fmt="d")
    plt.show()

    # plot train and test losses
    plt.plot(mlpw.train_losses, label="Train Loss")
    plt.plot(mlpw.test_losses, label="Test Loss")
    plt.legend()
    plt.show()

    # plot train and test accuracies
    plt.plot(mlpw.train_accuracies, label="Train Accuracy")
    plt.plot(mlpw.test_accuracies, label="Test Accuracy")
    plt.legend()
    plt.show()
