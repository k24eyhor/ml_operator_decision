import json
import matplotlib.pyplot as plt
from collections import Counter
import xarray as xr
import numpy as np

DATASET_NAME = "dataset_1.pkl"


def get_n_worse(data, n):
    return [(k, v) for k, v in sorted(data.items(), key=lambda item: item[1], reverse=True)][:n]

def get_values(data):
    return [v for _, v in data.items()]

def plot_distribution(data, label):
    counter = Counter(get_values(data))
    plt.bar(list(counter.keys()), list(counter.values()), color="skyblue")
    plt.xlabel('count of wrong classification', fontsize=14)
    plt.ylabel('Frequency', fontsize=14)
    plt.title(f'Wrong classification frequency ({label})', fontsize=16)
    plt.show()

def plot_6_worse(IDs, counts, dataset_path, graph = "TEMP"):
        
    fig, axes = plt.subplots(3, 2, figsize=(12, 12)) 
    
    X = []
    Y = []
    for i in IDs:
        real_name = "_".join(i.split("_")[:-1]) + ".nc"
        prof_num = int(i.split("_")[-1])
        ds = xr.open_dataset(dataset_path + "/" + real_name)
        X.append(np.array(ds['PRES'])[prof_num])
        Y.append(np.array(ds[graph])[prof_num])

    # Flatten the axes array for easy iteration
    axes = axes.flatten()

    # Loop through each pair of sublists and create a subplot
    for i in range(6):
        ax = axes[i]
        ax.plot(X[i], Y[i])
        ax.set_title(f'Profile {IDs[i]}, {counts[i]}% error')

    # Adjust layout to prevent overlap
    plt.tight_layout()

    # Show the plot
    plt.show()  

if __name__ == "__main__":
    false_negatives, false_positives = {}, {}
    with open('logs/false_negatives_2_v1.json', 'r') as json_file:
        false_negatives = json.load(json_file)

    with open('logs/false_positives_2_v1.json', 'r') as json_file:
        false_positives = json.load(json_file)
    worst_FN = get_n_worse(false_positives, 6)
    IDs = [x[0] for x in worst_FN] 
    counts = [x[1] for x in worst_FN] 
    plot_6_worse(IDs, counts, "datasets/dataset_2_v1/v1", graph = "PSAL")