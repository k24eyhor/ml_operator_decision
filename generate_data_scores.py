# TODO: Refactor as class and seperate dataloader.py
import torch.nn as nn
import pandas as pd
import numpy as np
import torch
from tqdm import trange, tqdm
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import seaborn as sns
import matplotlib.pyplot as plt
import torch.nn.functional as F
import json

S_features_filter = [
    "abs_S_Smin",
    "rel_S_Smin_semi_width",
    "rel_S_Smin_full_width",
    "abs_S_Smax",
    "rel_S_Smax_semi_width",
    "rel_S_Smax_full_width",
    "count_anomalies_S",
    "ratio_anomalies_S",
    "max_variation_S",
]
T_features_filter = [
    "abs_T_Tmin",
    "rel_T_Tmin_semi_width",
    "rel_T_Tmin_full_width",
    "abs_T_Tmax",
    "rel_T_Tmax_semi_width",
    "rel_T_Tmax_full_width",
    "count_anomalies_T",
    "ratio_anomalies_T",
    "max_variation_T",
]
B_features_filter = ["mean_correlation", "nb_measurements"]

PICKLE_PATH = "dataset_pandas/dataset_2_v2.pkl"
RANDOM_SEED = 123122334
# RANDOM_SEED = 42


##### HYPERPARAMETERS #####
EPOCHS = 200  # 350
BATCH_SIZE = 32  # 16
CRITERION = (
    nn.BCELoss()
)  # nn.BCEWithLogitsLoss(pos_weight=torch.tensor(0.005)) #nn.BCELoss()
OPTIMIZER = torch.optim.Adam  # torch.optim.SGD
# LEARNING_RATES = np.logspace(0.001, 0.0000001, 10)
LEARNING_RATE = 1e-4
GROWTH_RATE = 16  # 16
DROP_RATE = 0  # 0.2
SCHEDULER_PATIENCE = 10  # 15
SCHEDULER_FACTOR = 0.1
SCHEDULER_EPS = 1e-8

PLOTS = []

input_features = 22
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


##### MLP Definition #####
class MLP(nn.Module):
    def __init__(self):
        super(MLP, self).__init__()
        self.block1 = nn.Sequential(
            nn.Linear(input_features, GROWTH_RATE),
            # nn.BatchNorm1d(GROWTH_RATE),
            nn.ReLU(),
        )
        self.block2 = nn.Sequential(
            nn.Linear(GROWTH_RATE, GROWTH_RATE), nn.BatchNorm1d(GROWTH_RATE), nn.ReLU()
        )
        self.block3 = nn.Sequential(
            nn.Linear(2 * GROWTH_RATE, GROWTH_RATE),
            # nn.BatchNorm1d(GROWTH_RATE),
            nn.ReLU(),
        )
        self.block4 = nn.Sequential(
            nn.Linear(3 * GROWTH_RATE, GROWTH_RATE),
            # nn.BatchNorm1d(GROWTH_RATE),
            nn.ReLU(),
        )
        self.block5 = nn.Sequential(
            nn.Linear(4 * GROWTH_RATE, GROWTH_RATE),
            # nn.BatchNorm1d(GROWTH_RATE),
            nn.ReLU(),
        )
        self.block6 = nn.Sequential(
            nn.Linear(5 * GROWTH_RATE, GROWTH_RATE),
            # nn.BatchNorm1d(GROWTH_RATE),
            nn.ReLU(),
        )
        self.output = nn.Linear(GROWTH_RATE, 1)

    def forward(self, x):
        x1 = self.block1(x)
        x2 = self.block2(x1)
        x3 = self.block3(torch.cat([x1, x2], dim=1))
        x4 = self.block4(torch.cat([x1, x2, x3], dim=1))
        x5 = self.block5(torch.cat([x1, x2, x3, x4], dim=1))
        x6 = self.block6(torch.cat([x1, x2, x3, x4, x5], dim=1))
        y = self.output(x3)
        y = torch.sigmoid(y)
        return y


########## PREPROCESSING ############
def prepare_test_train_df(
    path=PICKLE_PATH, data_seed=RANDOM_SEED
) -> tuple[np.ndarray, np.ndarray]:
    # Load data
    df = pd.read_pickle(path)

    print("HEAD : ", df.head())

    # create a single output column that is one if either of the FalseorTrue columns is true
    df["FalseorTrue"] = df[["FalseorTrue_T", "FalseorTrue_S"]].any(axis=1).astype(int)

    # drop the individual FalseorTrue columns
    df = df.drop(columns=["FalseorTrue_T", "FalseorTrue_S"])

    # split the into training and testing sets
    train_df, test_df = train_test_split(df, test_size=0.2, random_state=data_seed)

    print("Train alarm distribution (before undersampling):")
    print(train_df["FalseorTrue"].value_counts())

    # calculate the number of samples in the minority class
    train_df = undersample(train_df, "FalseorTrue", data_seed)
    print("Train alarm distribution (after undersampling):")
    print(train_df["FalseorTrue"].value_counts())

    test_df = undersample(test_df, "FalseorTrue", data_seed)

    return train_df, test_df


def prepare_features_and_labels(
    train_df, test_df
) -> tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, list[str]]:

    # Separate train dataset into features and labels
    X_train = train_df.drop(columns=["FalseorTrue"]).values
    
    # set label to true when either of the alarms is true
    y_train = train_df["FalseorTrue"].values

    # Separate test dataset into features and labels
    X_test = test_df.drop(columns=["FalseorTrue"]).values
    y_test = test_df["FalseorTrue"].values

    X_train = X_train.astype(np.float32)
    y_train = y_train.astype(np.float32)

    X_test = X_test.astype(np.float32)
    y_test = y_test.astype(np.float32)
    return X_train, y_train, X_test, y_test, list(test_df.index)


def undersample(df, column, data_seed=RANDOM_SEED):
    # calculate the number of samples in the minority class
    min_FalseorTrue = df[column].value_counts().min()
    print(f"Min FalseorTrue: {min_FalseorTrue}")
    # Balance the training set
    df = pd.concat(
        [
            df[df[column] == 1].sample(min_FalseorTrue, random_state=data_seed),
            df[df[column] == 0],
        ]
    )
    return df


########## TRAINING ############
def train_model(
    X_train, y_train, X_test, y_test, model_seed=RANDOM_SEED, learning_rate=None
):

    # Setting up the data loader
    train_loader = torch.utils.data.DataLoader(
        list(zip(X_train, y_train)), batch_size=BATCH_SIZE, shuffle=True
    )

    # Setting up the test loader
    test_loader = torch.utils.data.DataLoader(
        list(zip(X_test, y_test)), batch_size=BATCH_SIZE, shuffle=False
    )

    # Fix the model seed for reproducibility
    torch.manual_seed(model_seed)

    # Define model
    model = MLP().to(device)

    # Set Dropout rate
    model.dropout = nn.Dropout(DROP_RATE)

    # Define loss function
    criterion = CRITERION

    # Define optimizer
    if learning_rate != None:
        optimizer = OPTIMIZER(model.parameters(), lr=learning_rate)
    else:
        optimizer = OPTIMIZER(model.parameters(), lr=LEARNING_RATE)

    # Define a Scheduler
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
        optimizer,
        mode="min",
        factor=SCHEDULER_FACTOR,
        patience=SCHEDULER_PATIENCE,
        eps=SCHEDULER_EPS,
    )

    # Train model
    train_losses = []
    test_losses = []
    train_accuracies = []
    test_accuracies = []
    for epoch in range(EPOCHS):
        epoch_train_loss = 0
        total_train = 0
        correct_train = 0
        with tqdm(train_loader, unit="batch") as t:
            model.train()
            for data, target in t:
                t.set_description(f"Epoch {str(epoch).rjust(5)}")

                # Move data to device and set model to train mode
                data, target = data.to(device), target.to(device)

                # Zero the gradients and forward pass
                output = model(data.float())

                # Calculate loss
                loss = criterion(output, target.float().view(-1, 1))
                epoch_train_loss += loss.item()

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                # Calculate accuracy
                y_train_pred_binary = np.where(output.data > 0.5, 1, 0).reshape(
                    1, len(output)
                )[0]
                correct_train += (target.numpy() == y_train_pred_binary).sum().item()
                total_train += len(target)

                # Display loss
                t.set_postfix(train_loss=f"{loss.item():.4f}")

        # Compute total train accuracy
        train_accuracy = 100 * correct_train / total_train
        train_accuracies.append(train_accuracy)

        print(f"Train accuracy: {train_accuracy}")

        # print optimizer learning rate
        print(f"Learning rate: {optimizer.param_groups[0]['lr']}")

        # compute train loss
        epoch_train_loss /= len(train_loader)

        train_losses.append(epoch_train_loss)

        # update scheduler
        scheduler.step(epoch_train_loss)

        # set model to evaluation mode
        model.eval()
        with torch.no_grad():
            test_loss = 0
            correct_test = 0
            total_test = 0
            for data, target in test_loader:
                data, target = data.to(device), target.to(device)
                output = model(data.float())
                loss = criterion(output, target.float().view(-1, 1))
                test_loss += loss.item()

                # Calculate accuracy
                y_test_pred_binary = np.where(output.data > 0.5, 1, 0).reshape(
                    1, len(output)
                )[0]
                correct_test += (target.numpy() == y_test_pred_binary).sum().item()
                total_test += len(target)

            test_loss /= len(test_loader)
            test_accuracy = 100 * correct_test / total_test

            test_accuracies.append(test_accuracy)
            print(f"Test accuracy: {test_accuracy}")

            test_losses.append(test_loss)
            print(f"Test loss: {test_loss:.4f}")

            # Early stop when the test accuracy is under 52 after 10 epochs
            if epoch > 10 and test_accuracy < 52:
                print("Early stopping, test accuracy under 52")
                break

            # Early stop when the test accuracy didn't deviate by more than 0.01 in all the last 5 epochs
            if epoch > 10 and all(
                abs(test_accuracies[-1] - test_accuracies[-i]) < 0.01
                for i in range(1, 5)
            ):
                print("Early stopping, test accuracy didn't improve")
                break

        torch.save(
            model.state_dict(),
            f"checkpoints/mlp.pth",
        )

    return model, train_losses, test_losses, train_accuracies, test_accuracies

def evaluate_with_ids(model, X_test, y_test, IDs, iter = 1):

    test_loader = torch.utils.data.DataLoader(
        list(zip(X_test, y_test)), batch_size=BATCH_SIZE, shuffle=False
    )

    # Define loss function
    criterion = CRITERION
    scores_per_id = {}
    model.eval()
    with torch.no_grad():
        test_loss = 0
        correct_test = 0
        total_test = 0
        index = 0
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data.float())
            loss = criterion(output, target.float().view(-1, 1))
            test_loss += loss.item()

            # Calculate accuracy
            y_test_pred_binary = np.where(output.data > 0.5, 1, 0).reshape(
                1, len(output)
            )[0]
            is_correct = (target.numpy() == y_test_pred_binary).sum().item()
            correct_test += is_correct
            total_test += len(target)
            scores_per_id[IDs[index]] = is_correct
            index += 1

        test_loss /= len(test_loader)
        test_accuracy = 100 * correct_test / total_test

        test_accuracies.append(test_accuracy)
        print(f"Test accuracy: {test_accuracy}")

        test_losses.append(test_loss)
        print(f"Test loss: {test_loss:.4f}")
    return scores_per_id
        
########## POST TRAINING & EVALUATION ############

def load_model_from_checkpoint(model, test_losses):
    # load best model from checkpoint
    print(f"Loading model from checkpoint: mlp.pth")
    model.load_state_dict(torch.load(f"checkpoints/mlp.pth"))
    return model


def get_evaluation(model, X_test):
    # predict on test set
    model.eval()
    with torch.no_grad():
        y_test_pred = (
            model(torch.tensor(X_test).float().to(device)).cpu().detach().numpy()
        )
    y_test_pred_binary = np.where(y_test_pred > 0.5, 1, 0)

    # print parameter count of model
    # print(f"Parameter count: {sum(p.numel() for p in model.parameters())}")

    return y_test_pred, y_test_pred_binary


def get_confusion_matrix(y_test_pred_binary, y_test):
    # calculate confusion matrix
    cm = confusion_matrix(y_test, y_test_pred_binary)
    return cm


def get_metrics(cm):
    # calculate accuracy
    accuracy = np.sum(np.diag(cm)) / np.sum(cm)
    print(f"Accuracy: {accuracy}")

    # calculate recall
    recall = cm[1, 1] / (cm[1, 0] + cm[1, 1])
    print(f"Recall: {recall}")

    # calculate precision
    precision = cm[1, 1] / (cm[0, 1] + cm[1, 1])
    print(f"Precision: {precision}")

    # calculate F1 score
    f1 = 2 * (precision * recall) / (precision + recall)
    print(f"F1 score: {f1}")

    # calculate F2 score
    f2 = 5 * (precision * recall) / (4 * precision + recall)
    print(f"F2 score: {f2}")

    return accuracy, precision, recall, f1, f2


def sort_testdata_into_cm(test_df, y_test_pred, y_test_pred_binary):
    # Function to determine TP, TN, FP, FN
    def determine_result(row):
        if row["FalseorTrue"] == 1 and row["predicted"] == 1:
            return "TP"
        elif row["FalseorTrue"] == 0 and row["predicted"] == 0:
            return "TN"
        elif row["FalseorTrue"] == 0 and row["predicted"] == 1:
            return "FP"
        elif row["FalseorTrue"] == 1 and row["predicted"] == 0:
            return "FN"

    # Sort the predictions into CM categories
    y_pred_series = pd.Series(y_test_pred.reshape(1, -1)[0])
    y_pred_binary_series = pd.Series(y_test_pred_binary.reshape(1, -1)[0])
    results = test_df.copy()
    y_pred_series.index = results.index
    y_pred_binary_series.index = results.index
    results["predicted"] = y_pred_binary_series.astype(bool)
    results["predicted_raw"] = y_pred_series

    # Create new column based on conditions for the confusion matrix
    results["CM"] = results.apply(determine_result, axis=1)

    return results


if __name__ == "__main__":
    false_positives = {}
    false_negatives = {}
    for seed in range(100):
        train_df, test_df = prepare_test_train_df(data_seed=seed)

        X_train, y_train, X_test, y_test, indexes_test = prepare_features_and_labels(train_df, test_df)
        model, train_losses, test_losses, train_accuracies, test_accuracies = train_model(
            X_train, y_train, X_test, y_test, learning_rate=LEARNING_RATE
        )
        scores = evaluate_with_ids(model, X_test, y_test, indexes_test)
        print(scores)
        # model = load_model_from_checkpoint(model, test_losses)
        y_test_pred, y_test_pred_binary = get_evaluation(model, X_test)

        """
        cm = get_confusion_matrix(y_test_pred_binary, y_test)
        sns.heatmap(cm, annot=True, fmt="d")
        plt.show()

        # plot train and test losses
        plt.plot(train_losses, label="Train Loss")
        plt.plot(test_losses, label="Test Loss")

        plt.legend()
        plt.show()

        # plot train and test accuracies
        plt.plot(train_accuracies, label="Train Accuracy")
        plt.plot(test_accuracies, label="Test Accuracy")

        plt.legend()
        plt.show()
        """
        # accuracy, precision, recall, f1, f2 = get_metrics(cm)
        results = sort_testdata_into_cm(test_df, y_test_pred, y_test_pred_binary)
        print(results)
        for index, row in results.iterrows():
            if row["CM"] == "FP":
                increment_dict_key(false_positives, index)
            if row["CM"] == "FN":
                increment_dict_key(false_negatives, index)
        # print("Done!")

        # do the confusion matrix for train data
        y_train_pred, y_train_pred_binary = get_evaluation(model, X_train)
        cm_train = get_confusion_matrix(y_train_pred_binary, y_train)

        # plot confusion matrix
        # sns.heatmap(cm_train, annot=True, fmt="d")
        # plt.show()
    # Save results to JSON files
    with open('./logs/false_positives_2_v2.json', 'w') as json_file:
        json.dump(false_positives, json_file)
    with open('./logs/false_negatives_2_v2.json', 'w') as json_file:
        json.dump(false_negatives, json_file)