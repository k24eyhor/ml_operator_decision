import pandas as pd
import argparse
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.figure import Figure

# Add arguments for CSV file path and output path
parser = argparse.ArgumentParser()
parser.add_argument("--csv", help="path to the csv file", type=str)
parser.add_argument("--output", help="path to the output file", type=str)
parser.add_argument(
    "--type",
    help="Either single, 4x1 or 4x3",
    type=str,
    default=False,
)
args = parser.parse_args()

# If type = 4x1 argument, this is the list of CSV files to be used
csv_paths_4_1 = [
    "logs/v1_random_data_seed.csv",
    "logs/v2_random_data_seed.csv",
    "logs/v3_random_data_seed.csv",
    "logs/v4_random_data_seed.csv",
]

# If type = 4x3 argument, this is the list of CSV files to be used
csv_paths_4_3 = [
    "logs/v1_temp_random_data_seed.csv",
    "logs/v1_sal_random_data_seed.csv",
    "logs/v1_random_data_seed.csv",
    "logs/v2_temp_random_data_seed.csv",
    "logs/v2_sal_random_data_seed.csv",
    "logs/v2_random_data_seed.csv",
    "logs/v3_temp_random_data_seed.csv",
    "logs/v3_sal_random_data_seed.csv",
    "logs/v3_random_data_seed.csv",
    "logs/v4_temp_random_data_seed.csv",
    "logs/v4_sal_random_data_seed.csv",
    "logs/v4_random_data_seed.csv",
]


def create_histogram(csv_path) -> Figure:
    """Create a histogram of the accuracy from a single csv file."""
    # Read the CSV file
    df = pd.read_csv(csv_path, sep=", ")

    # Convert accuracy to percentage
    df["accuracy"] = df["accuracy"] * 100

    fig = plt.figure(figsize=(10, 6))

    # Plot the histogram with KDE
    sns.histplot(df, x="accuracy", kde=False, bins=20, stat="density")

    # Use the next color in the palette
    sns.kdeplot(df["accuracy"], color=sns.color_palette()[1])

    # Set labels and limits
    plt.xlabel("Accuracy (%)")
    plt.ylabel("Frequency")
    plt.xlim(75, 95)

    return fig


def histogram_4_1(csv_paths) -> Figure:
    """
    Create a 1x4 subplots for the 4 CSV files with horizontal titles on
    the left side of each subplot and a shared x-axis.
    """
    fig, ax = plt.subplots(4, 1, figsize=(10, 10), sharex=True)

    for i, path in enumerate(csv_paths):
        df = pd.read_csv(path, sep=", ", engine="python")
        df["accuracy"] = df["accuracy"] * 100

        sns.histplot(df, x="accuracy", kde=False, bins=20, stat="density", ax=ax[i])
        sns.kdeplot(df["accuracy"], color=sns.color_palette()[1], ax=ax[i])

        # Add the title horizontally on the left side
        ax[i].text(
            -0.12,
            0.5,
            f"Dataset_V{i+1}",
            va="center",
            ha="center",
            rotation=90,
            transform=ax[i].transAxes,
            fontsize=14,
        )

        ax[i].set_ylabel("Frequency")
        ax[i].set_xlim(79, 91)

    # Set the xlabel for the bottom subplot only
    ax[-1].set_xlabel("Accuracy (%)")

    fig.tight_layout()
    return fig


def histogram_4_3(csv_paths) -> Figure:
    """
    Create a 4x3 subplots for the 12 CSV files, row-wise dataset type, column-wise: (temp, sal, both)
    """
    fig, ax = plt.subplots(4, 3, figsize=(13, 8), sharex=True, sharey=True)

    dataset_labels = ["v1", "v2", "v3", "v4"]
    type_labels = ["Temperature", "Salinity", "Both"]

    # Determine overall range and bins
    all_data = []
    for path in csv_paths:
        df = pd.read_csv(path, sep=", ", engine="python")
        df["accuracy"] = df["accuracy"] * 100
        all_data.extend(df["accuracy"].tolist())

    bin_edges = pd.cut(all_data, bins=100).categories.right

    for i in range(4):  # Rows
        for j in range(3):  # Columns
            idx = i * 3 + j
            df = pd.read_csv(csv_paths[idx], sep=", ", engine="python")
            df["accuracy"] = df["accuracy"] * 100

            sns.histplot(
                df, x="accuracy", kde=False, bins=bin_edges, stat="density", ax=ax[i, j]
            )
            sns.kdeplot(df["accuracy"], color=sns.color_palette()[1], ax=ax[i, j])
            ax[i, j].set_xlim(60, 90)

            if i == 3:  # Bottom row
                ax[i, j].set_xlabel("Accuracy (%)")
            if j == 0:  # Leftmost column
                ax[i, j].set_ylabel("Frequency")

            # Add row titles
            if j == 0:
                ax[i, j].text(
                    -0.25,
                    0.5,
                    f"Dataset_{dataset_labels[i]}",
                    va="center",
                    ha="center",
                    rotation=90,
                    transform=ax[i, j].transAxes,
                    fontsize=14,
                )
            # Add column titles
            if i == 0:
                ax[i, j].set_title(type_labels[j], fontsize=14)

    fig.tight_layout()
    return fig


if __name__ == "__main__":
    if args.type == "single":
        fig = create_histogram(args.csv)
    elif args.type == "4x1":
        fig = histogram_4_1(csv_paths=csv_paths_4_1)
    elif args.type == "4x3":
        fig = histogram_4_3(csv_paths=csv_paths_4_3)

    if args.output is not None:
        fig.savefig(args.output, dpi=300, bbox_inches="tight")
