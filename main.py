import mlp as m
import plot_results as p
from dataloader import DataLoader

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import confusion_matrix
import argparse
import json

args = argparse.ArgumentParser()
args.add_argument("--run", type=str, default="single")

args = args.parse_args()


def run_and_plot_distributions():
    """Trains the model once with fixed seeds and plots the results."""
    FIX_MODEL_SEED = 42
    FIX_DATA_SEED = 42
    VERSION = "v2_test"

    # Instantiate the DataLoader with the desired parameters
    dl = DataLoader(
        variant="v1",
        features="both",
        batch_size=32,
        seed=FIX_DATA_SEED,
        rebalance_train=True,
        rebalance_test=True,  ## Gives you a better idea of the model's performance
    )

    # Instantiate the MLPWrapper with the desired parameters
    mlpw = m.MLPWrapper(
        input_features=dl.num_features,
        growth_rate=16,
        train_loader=dl.train_loader,
        test_loader=dl.test_loader,
        learning_rate=1e-4,
        model_seed=FIX_MODEL_SEED,
        device="cpu",
    )

    # Train the model
    mlpw.train(epochs=200)

    #### Evaluation of the model ####
    y_test_pred, y_test_pred_binary = m.get_evaluation(mlpw.model, dl.X_test)

    cm = confusion_matrix(y_test_pred_binary, dl.y_test)
    results = m.sort_testdata_into_cm(dl.test_df, y_test_pred, y_test_pred_binary)

    accuracy, precision, recall, f1, f2 = m.get_metrics(cm)

    fig, ax = plt.subplots(2, 2, figsize=(24, 6))
    p.plot_cm(cm, ax[0, 0])
    p.plot_train_test_evolution(
        mlpw.train_accuracies, mlpw.test_accuracies, ax[0, 1], metric="accuracy"
    )
    p.plot_train_test_evolution(mlpw.train_losses, mlpw.test_losses, ax[1, 0])

    sns.histplot(data=results, x="predicted_raw", hue="CM", ax=ax[1, 1], stat='percent')
    ax[1, 1].set_xlabel('Predicted values')
    ax[1, 1].set_ylabel('Distribution (%)')

    plt.savefig(
        f"./results/no_bn/results_{VERSION}_data{FIX_DATA_SEED}_model{FIX_MODEL_SEED}.png"
    )
    plt.show()


def multiple_runs():
    """Trains the model with different seeds for the data and saves the results in a CSV file."""
    FIX_MODEL_SEED = 42
    CSV_NAME = "v1_random_data_seed"
    NUM_RUNS = 100

    for data_seed in np.random.randint(0, 10000, size=NUM_RUNS):
        model_seed = FIX_MODEL_SEED
        dl = DataLoader(
            variant="v1",
            features="both",
            batch_size=32,
            seed=data_seed,
            rebalance_train=True,
            rebalance_test=True,  ## Gives you a better idea of the model's performance
        )

        mlpw = m.MLPWrapper(
            input_features=dl.num_features,
            growth_rate=16,
            train_loader=dl.train_loader,
            test_loader=dl.test_loader,
            learning_rate=1e-4,
            model_seed=model_seed,
            device="cpu",
        )

        mlpw.train(epochs=200)
        y_test_pred, y_test_pred_binary = m.get_evaluation(mlpw.model, dl.X_test)

        cm = m.get_confusion_matrix(y_test_pred_binary, dl.y_test)

        accuracy, precision, recall, f1, f2 = m.get_metrics(cm)

        p.save_metrics(
            CSV_NAME, data_seed, model_seed, accuracy, recall, precision, f1, f2
        )

def generate_scores():
    """
    This function generates a json file counting how many times 
    each profile was classified wrong over N_RUNS runs
    """

    # Parameters
    N_RUNS = 1
    DATASET_NAME = "2_v2" # For the saved JSON names, you should change the actual dataset in mlp.py

    def increment_dict_key(d, key):
        if key in d.keys():
            d[key] += 1
        else:
            d[key] = 1

    false_positives = {}
    false_negatives = {}

    # Trainings loop
    for seed in range(N_RUNS):
            
        # Instantiate the DataLoader with the desired parameters
        dl = DataLoader(
            variant="v1",
            features="both",
            batch_size=32,
            seed=seed,
            rebalance_train=True,
            rebalance_test=True,  ## Gives you a better idea of the model's performance
        )

        # Instantiate the MLPWrapper with the desired parameters
        mlpw = m.MLPWrapper(
            input_features=dl.num_features,
            growth_rate=16,
            train_loader=dl.train_loader,
            test_loader=dl.test_loader,
            learning_rate=1e-4,
            model_seed=seed,
            device="cpu",
        )
        mlpw.train(epochs=20)
        y_test_pred, y_test_pred_binary = m.get_evaluation(mlpw.model, dl.X_test)

        results = m.sort_testdata_into_cm(dl.test_df, y_test_pred, y_test_pred_binary)
        for index, row in results.iterrows():
            if row["CM"] == "FP":
                increment_dict_key(false_positives, index)
            if row["CM"] == "FN":
                increment_dict_key(false_negatives, index)

    # Save results to JSON files
    with open(f'./logs/false_positives_{DATASET_NAME}.json', 'w') as json_file:
        json.dump(false_positives, json_file)
    with open(f'./logs/false_negatives_{DATASET_NAME}.json', 'w') as json_file:
        json.dump(false_negatives, json_file)


if __name__ == "__main__":
    if args.run == "single":
        run_and_plot_distributions()
    elif args.run == "multiple":
        multiple_runs()
    elif args.run == "generate_scores":
        generate_scores()
