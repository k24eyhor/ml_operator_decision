import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import StandardScaler


######### PLOTTING FUNCTIONS ########
def plot_train_test_evolution(train_losses, test_losses, ax, metric="loss"):
    """
    Function to plot the evolution of the train and test losses (by default) over the training process.

    If metric = "Accuracy", this plots the evolution of the accuracy.

    Parameters:
    -----------
    train_losses : list of floats
        The list of training losses
    test_losses : list of floats
        The list of testing losses
    ax : pyplot.ax
        Axis on which to have the final plot
    metric:
        - "loss" to plot the losses
        - "accuracy" to plot the accuracies
    """
    sns.lineplot(
        x=range(len(train_losses)), y=train_losses, label=f"Train {metric}", ax=ax
    )
    sns.lineplot(
        x=range(len(test_losses)), y=test_losses, label=f"Test {metric}", ax=ax
    )

    if metric == "loss":
        # make axis log scale
        ax.set_yscale("log")

    ax.set_xlabel("Epoch")
    ax.set_ylabel(metric)


def plot_cm(cm, ax):
    """
    Function to plot the confusion matrix in a form of a heatmap

    Parameters:
    -----------
    cm: nd.array
        confusion matrix as a 2D array
    ax : pyplot.ax
        Axis on which to have the final plot
    """
    sns.heatmap(cm, annot=True, fmt="d", ax=ax)
    ax.set_xlabel("Predicted")
    ax.set_ylabel("True")


###################### LOGGING FUNCTIONS
def save_metrics(filename, data_seed, model_seed, accuracy, recall, precision, f1, f2):
    """
    Function to add a line in the csv file fo the logs

    Parameters:
    -----------
    filename: string
        Filename where to add the metric.If not available, it will be created.
    data_seed: int
        seed used for the data split
    model_seed: int
        seed used in pytorch for the intialization of the model
    accuracy, recall, precision, f1, f2: *string
        metrics calculated after training
    """
    with open(f"./logs/{filename}.csv", "a") as f:
        # when the file is created, add the header
        if f.tell() == 0:
            f.write("data_seed, model_seed, accuracy, recall, precision, f1, f2\n")
        f.write(
            f"{data_seed}, {model_seed}, {accuracy}, {recall}, {precision}, {f1}, {f2}\n"
        )
