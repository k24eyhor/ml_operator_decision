# New Datasets from Jerome

- `1_sal`: Old Dataset (Only Salinity Alarms)
- `1_temp`: Old Dataset (Only Temperature Alarms)
- `2_v0`: New Dataset (Salinity and Temperature Alarms)
- `2_v1`: New Dataset (Salinity and Temperature Alarms, Corrected Version (?))
- `2_v2`: New Dataset (Salinity and Temperature Alarms, WF multiplied by 2/3)
- `2_v3`: New Dataset (Salinity and Temperature Alarms, WF multiplied by 1/3)
- `2_v4`: New Dataset (Salinity and Temperature Alarms, WF multiplied by 0)

| Name   | No. Profiles | No. Sal Alarms | Sal False Rate | No. Temp Alarms | Temp False Rate |
| ------ | ------------ | -------------- | -------------- | --------------- | --------------- |
| 1_sal  | 2566         | 2566           | 11.8 %         | -               | -               |
| 1_temp | 419          | -              | -              | 419             | 43.19 %         |
| 2_v0   | 41527        | 39805          | 2.19 %         | 3954            | 11.48 %         |
| 2_v1   | 41323        | 39583          | 2.28 %         | 3661            | 12.23 %         |
| 2_v2   | 43813        | 41467          | 3.08 %         | 4158            | 17.26 %         |
| 2_v3   | 48418        | 44589          | 4.52 %         | 5374            | 27.02 %         |
| 2_v4   | 61140        | 51430          | 8.00 %         | 10357           | 41.80 %         |

## Feature extraction of the new datasets

- Trying to keep both salinity and temperature alarms in the same dataset to increase the number of profiles.
- Same features as before
- New features:
  - `alarm_salinity`: Alarm raised because of Salinity
  - `alarm_temperature`: Alarm raised because of Temperature
- Label: `TrueorFalse` (`TrueorFalse_S` OR `TrueorFalse_T`)

## Observations

- The new datasets have a lot more profiles than the old ones
- BUT The false rates are much lower than the old datasets -> very unbalanced especially for the salinity alarms
- After Undersampling the majority class, we lose a lot of profiles :/
- Initially, same problems as before -> Performance very dependent on the test set on all datasets
- Unexpected high "jumps" in test loss:  
![alt text](image.png){width=40%}
- Even when we have a train accuracy of around 80%, after calling model.eval() the model predicts everything as the majority class (True Alarm):  
![alt text](image-1.png){width=40%}
- Investigating this further showd that removing BatchNorm layers increases the performance on the test set by a lot (result of the test set being very different from the training set (?)):  
![alt text](image-2.png){width=40%}  
![alt text](image-3.png){width=40%}
- Still no convergence for all seeds.. but more stable results without BatchNorm layers

## Results from first quick tests

Same Seed, 100 Epochs, No BatchNorm Layers, Undersampling on both the training and test set (for better comparison)

- `2_v0`: 84.3 %
- `2_v1`: 86.5 %
- `2_v2`: 83.4 %
- `2_v3`: 82.8 %
- `2_v4`: 81.5 %
